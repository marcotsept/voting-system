const SERVER_URL = process.env.REACT_APP_SERVER_URL || ".";
const LOCAL_STORAGE_KEY = "voter";
export { SERVER_URL, LOCAL_STORAGE_KEY };
