import { Configuration, VoterApi, VoteApi, CampaignApi } from "../generated";
import { SERVER_URL } from "./const";

const voterApi = new VoterApi(new Configuration({ basePath: SERVER_URL }));
const voteApi = new VoteApi(new Configuration({ basePath: SERVER_URL }));
const campaignApi = new CampaignApi(new Configuration({ basePath: SERVER_URL }));
export { voterApi, voteApi, campaignApi };
