/* tslint:disable */
/* eslint-disable */
/**
 * API
 * API description
 *
 * OpenAPI spec version: 1.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface CampaignCandidate
 */
export interface CampaignCandidate {
  /**
   *
   * @type {Candidate}
   * @memberof CampaignCandidate
   */
  candidate: any;
  /**
   *
   * @type {number}
   * @memberof CampaignCandidate
   */
  totalVotes: any;
  /**
   *
   * @type {number}
   * @memberof CampaignCandidate
   */
  seq: any;
}
