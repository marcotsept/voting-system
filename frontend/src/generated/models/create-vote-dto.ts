/* tslint:disable */
/* eslint-disable */
/**
 * API
 * API description
 *
 * OpenAPI spec version: 1.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface CreateVoteDto
 */
export interface CreateVoteDto {
  /**
   *
   * @type {string}
   * @memberof CreateVoteDto
   */
  campaign: any;
  /**
   *
   * @type {string}
   * @memberof CreateVoteDto
   */
  voter: any;
  /**
   *
   * @type {string}
   * @memberof CreateVoteDto
   */
  candidate: any;
}
