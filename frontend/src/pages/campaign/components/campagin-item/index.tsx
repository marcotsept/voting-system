import React from "react";
import moment from "moment";
import { Campaign, CampaignCandidate } from "../../../../generated/models";
import { Button } from "@material-ui/core";
import { voteApi } from "../../../../common/api";
import { LOCAL_STORAGE_KEY } from "../../../../common/const";

interface CampaignItemProps {
  index: number;
  campaign: Campaign;
  vote: undefined | { [key: string]: string };
}

const CampaignItem = (props: CampaignItemProps) => {
  const { index } = props;
  const [campaign, setCampaign] = React.useState(props.campaign);
  const [vote, setVote] = React.useState(props.vote);

  const makeVote = React.useCallback(
    (candidate: string) => {
      voteApi
        .voteControllerCreateVote({
          voter: localStorage.getItem(LOCAL_STORAGE_KEY),
          candidate: candidate,
          campaign: campaign.id,
        })
        .then((res) => {
          if (res.status !== 201) return;
          setCampaign(res.data.campaign);
          setVote(res.data.candidate.id);
        });
    },
    [campaign],
  );

  return (
    <div>
      <p>
        Voting Campaign {index}: {campaign.name}
      </p>
      <p>
        Start: {moment(campaign.startDate).format("MMM D, YYYY")} End: {moment(campaign.endDate).format("MMM D, YYYY")}
      </p>
      {campaign.candidates.map((c: CampaignCandidate) => {
        if (vote || moment(campaign.endDate).isBefore(moment())) {
          return (
            <p key={c.candidate.id}>
              {c.seq}. {c.candidate.name} Vote: {c.totalVotes} {c.candidate.id === vote && <span>(You voted)</span>}
            </p>
          );
        } else {
          return (
            <p key={c.candidate.id}>
              {c.seq}. {c.candidate.name}{" "}
              <Button variant="contained" color="default" onClick={makeVote.bind(null, c.candidate.id)}>
                vote
              </Button>
            </p>
          );
        }
      })}
    </div>
  );
};

export { CampaignItem };
