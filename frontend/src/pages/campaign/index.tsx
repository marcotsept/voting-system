import React, { useState, useEffect } from "react";
import { LOCAL_STORAGE_KEY, SERVER_URL } from "../../common/const";
import { useHistory } from "react-router";
import { campaignApi, voterApi } from "../../common/api";
import { Campaign as CampaignType, Vote } from "../../generated/models";
import { makeStyles, Paper } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { CampaignItem } from "./components/campagin-item";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(3, 2),
  },
}));

const Campaign = () => {
  const history = useHistory();
  const [voter] = useState(localStorage.getItem(LOCAL_STORAGE_KEY));
  const [campaigns, setCampaigns] = React.useState<CampaignType[]>([]);
  const [votes, setVotes] = React.useState({});
  const classes = useStyles();

  useEffect(() => {
    if (!voter) return;
    const source = new EventSource(`${SERVER_URL}/campaigns/sse`);
    source.onmessage = function logEvents(event) {
      const result = JSON.parse(event.data);
      voterApi.voterControllerGetVotes(voter).then((res) => {
        mapVoteRecord(res.data);
        setCampaigns([]);
        setCampaigns([...result]);
      });
    };
  }, [voter]);

  useEffect(() => {
    const getData = async () => {
      if (!voter) return;
      Promise.all([voterApi.voterControllerGetVotes(voter), campaignApi.campaignControllerGetAll()]).then((result) => {
        mapVoteRecord(result[0].data);
        setCampaigns(result[1].data);
      });
    };
    getData();
  }, [voter]);

  if (!voter) {
    setTimeout(() => {
      history.push("/");
    }, 0);
    return <></>;
  }

  const mapVoteRecord = (data: Vote[]) => {
    setVotes(
      data.reduce((p, c) => {
        p[c.campaign.id] = c.candidate.id;
        return p;
      }, {}),
    );
  };

  return (
    <Grid container>
      <Grid item sm={12}>
        {campaigns.map((c, idx) => {
          return (
            <Paper className={classes.root} key={c.id}>
              <CampaignItem index={idx + 1} campaign={c} vote={votes[c.id]} />
            </Paper>
          );
        })}
      </Grid>
    </Grid>
  );
};

export { Campaign };
