import React from "react";
import { Button, TextField, Paper, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { voterApi } from "../../common/api";
import { useHistory } from "react-router";
import { LOCAL_STORAGE_KEY } from "../../common/const";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  root: {
    padding: theme.spacing(3, 2),
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 400,
  },
  errMsg: {
    color: "red",
  },
}));

const HKIDForm = () => {
  const classes = useStyles();
  const history = useHistory();
  const [hkid, setHKID] = React.useState("");
  const [errMsg, setErrMsg] = React.useState("");

  const handleSubmit = (evt) => {
    evt.preventDefault();

    setErrMsg("");

    if (!hkid.trim()) {
      setErrMsg("Please input HKID!");
      return;
    }

    voterApi
      .voterControllerCreate({ HKID: hkid })
      .then((res) => {
        if (res.status !== 201) return;
        localStorage.setItem(LOCAL_STORAGE_KEY, res.data.id);
        history.push("/campaign-site");
      })
      .catch((e) => {
        if (!e.response) console.error(e);
        else setErrMsg(e.response.data.message);
      });
  };

  return (
    <div>
      <Paper className={classes.root}>
        <Typography variant="h5" component="h3">
          Voting System
        </Typography>
        <form onSubmit={handleSubmit}>
          <TextField
            label="HKID"
            className={classes.textField}
            helperText="Enter your HKID"
            value={hkid}
            onChange={(e) => setHKID(e.target.value)}
          />
          <Button type="submit" variant="contained" color="primary" className={classes.button}>
            Submit
          </Button>

          {errMsg && <p className={classes.errMsg}>{errMsg}</p>}
        </form>
      </Paper>
    </div>
  );
};

export { HKIDForm };
