import React from "react";
import { Route, Switch } from "react-router-dom";
import { HKIDForm } from "../pages/HKID";
import { Campaign } from "../pages/campaign";

const Router = () => {
  return (
    <Switch>
      <Route component={HKIDForm} exact path="/" />
      <Route component={Campaign} exact path="/campaign-site" />
    </Switch>
  );
};

export { Router };
