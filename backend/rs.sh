#!/bin/bash

init_rs() {
  mongo --host mongo1 --port 27017 -u root -p root --eval "rs.initiate({ _id: 'rs0', members: [{ _id: 0, host: '${HOST}:27017' }] })"
}

until mongo --host mongo1
do echo 'waiting for mongo1'; sleep 2; done;

init_rs

echo "rs initiating finished"
exit 0
