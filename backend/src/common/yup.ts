import * as Yup from 'yup';
import { Types } from 'mongoose';

declare module 'yup' {
  interface StringSchema {
    checkObjectId(msg?: string): StringSchema;
  }
}

function checkObjectId(this: Yup.StringSchema, msg = 'Invalid ObjectId') {
  return this.test('checkObjectId', msg, function(v) {
    return Types.ObjectId.isValid(v);
  });
}

Yup.addMethod(Yup.string, 'checkObjectId', checkObjectId);

export const yup = Yup;
