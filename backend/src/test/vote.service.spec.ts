import { Model } from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { getConnectionToken, getModelToken } from '@nestjs/mongoose';
import { VoteService } from '../vote/vote.service';
import { IVote, Vote } from '../schema/vote.schema';
import { CampaignService } from '../campaign/campaign.service';
import { CreateVoteDto } from '../vote/dto';
import { HttpException } from '@nestjs/common';
import moment = require('moment');

describe('VoteService', () => {
  let service: VoteService;
  let campaignService: CampaignService;
  let model: Model<IVote>;
  const params: CreateVoteDto = {
    campaign: '5fd4a66c709700a64488db19',
    voter: '5fd4a66c709700a64488db19',
    candidate: '5fd4a66c709700a64488db19',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VoteService,
        {
          provide: CampaignService,
          useValue: {
            getCampaignById: jest.fn(),
          },
        },
        {
          provide: getConnectionToken('Database'),
          useValue: {},
        },
        {
          provide: getModelToken(Vote.name),
          useValue: {
            find: jest.fn(),
            exec: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<VoteService>(VoteService);
    campaignService = module.get<CampaignService>(CampaignService);
    model = module.get<Model<IVote>>(getModelToken(Vote.name));
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('make a vote', () => {
    it('campaign not found', async () => {
      jest
        .spyOn<CampaignService, any>(campaignService, 'getCampaignById')
        .mockReturnValueOnce(null);
      try {
        expect(await service.createVote(params)).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.message).toEqual('campaign not found!');
      }
    });

    it('candidate does not exist on campaign', async () => {
      jest
        .spyOn<CampaignService, any>(campaignService, 'getCampaignById')
        .mockReturnValueOnce(null);
      try {
        expect(await service.createVote(params)).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.message).toEqual('campaign not found!');
      }
    });

    it('campaign have ended', async () => {
      jest
        .spyOn<CampaignService, any>(campaignService, 'getCampaignById')
        .mockReturnValueOnce({
          endDate: moment()
            .add(-5, 'd')
            .toDate(),
          candidates: [{ candidate: { id: '5fd4a66c709700a64488db20' } }],
        });
      try {
        expect(await service.createVote(params)).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.message).toEqual('candidate does not exist on campaign!');
      }
    });

    it('you have voted', async () => {
      jest
        .spyOn<CampaignService, any>(campaignService, 'getCampaignById')
        .mockReturnValueOnce({
          endDate: moment()
            .add(5, 'd')
            .toDate(),
          candidates: [{ candidate: { id: '5fd4a66c709700a64488db19' } }],
        });
      jest
        .spyOn<Model<IVote>, any>(model, 'find')
        .mockReturnValueOnce([{ _id: 'A001' }]);

      try {
        expect(await service.createVote(params)).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
        expect(e.message).toEqual('you have voted!');
      }
    });
  });
});
