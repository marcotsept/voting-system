import { CreateVotePipe } from '../vote/create-vote.pipe';
import { HttpException } from '@nestjs/common';

describe('CreateVotePipe', () => {
  let pipe: CreateVotePipe;

  describe('validation CreateVoteDto', () => {
    beforeEach(() => {
      pipe = new CreateVotePipe();
    });

    it('invalid campaign', async () => {
      try {
        expect(
          await pipe.transform(
            {
              campaign: 'A1',
              voter: '5fd4a66c709700a64488db19',
              candidate: '5fd4a66c709700a64488db19',
            },
            {} as any,
          ),
        ).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
      }
    });

    it('invalid voter', async () => {
      try {
        expect(
          await pipe.transform(
            {
              campaign: '5fd4a66c709700a64488db19',
              voter: 'A1',
              candidate: '5fd4a66c709700a64488db19',
            },
            {} as any,
          ),
        ).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
      }
    });

    it('invalid candidate', async () => {
      try {
        expect(
          await pipe.transform(
            {
              campaign: '5fd4a66c709700a64488db19',
              voter: '5fd4a66c709700a64488db19',
              candidate: 'A1',
            },
            {} as any,
          ),
        ).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
      }
    });

    it('pass', async () => {
      const params = {
        campaign: '5fd4a66c709700a64488db19',
        voter: '5fd4a66c709700a64488db19',
        candidate: '5fd4a66c709700a64488db19',
      };
      expect(await pipe.transform(params, {} as any)).toEqual(params);
    });
  });
});
