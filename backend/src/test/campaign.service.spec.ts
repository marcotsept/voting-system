import { CampaignService } from '../campaign/campaign.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { CampaignModule } from '../campaign/campaign.module';
import { HttpException } from '@nestjs/common';
import { Campaign } from '../schema/campaign.schema';
import { Candidate } from '../schema/candidate.schema';
import { mongoServer } from './common';

describe('CampaignService', () => {
  let campaignService: CampaignService;
  let campaign: Campaign;

  beforeAll(async () => {
    await mongoServer.start();
    const mongoUri = await mongoServer.getUri();
    const mongoModule = MongooseModule.forRoot(mongoUri);
    const module = await Test.createTestingModule({
      imports: [mongoModule, CampaignModule],
    }).compile();
    campaignService = module.get<CampaignService>(CampaignService);
  });

  afterAll(async () => await mongoServer.stop());

  it('successful create and get one or all', async () => {
    campaign = await campaignService.createCampaignCandidateForTest();
    const campaigns = await campaignService.getAll();
    expect(campaigns.length).toBeGreaterThan(0);
    expect(campaigns[0].id.toString()).toEqual(campaign.id.toString());

    const getOneCampaign = await campaignService.getCampaignById(campaign.id);
    expect(getOneCampaign.id).toEqual(campaign.id);
  });

  it('should be throw error campaign is existed', async () => {
    await campaignService.createCampaignCandidateForTest().catch(err => {
      expect(err).toBeInstanceOf(HttpException);
    });
  });

  it('successful update number of totalVote', async () => {
    const candidate = (campaign.candidates[0].candidate as Candidate).id;
    const updated = await campaignService.makeVote(
      campaign.id,
      candidate,
      null,
    );
    const getOneCampaign = await campaignService.getCampaignById(campaign.id);
    expect(updated.nModified).toEqual(1);
    expect(campaign.candidates[0].totalVotes + 1).toEqual(
      getOneCampaign.candidates[0].totalVotes,
    );
  });
});
