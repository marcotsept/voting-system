import { CreateVoterPipe } from '../voter/create-voter.pipe';
import { HttpException } from '@nestjs/common';

describe('CreateVoterPipe', () => {
  let pipe: CreateVoterPipe;

  describe('validation HKID', () => {
    beforeEach(() => {
      pipe = new CreateVoterPipe();
    });

    it('invalid HKID', async () => {
      try {
        expect(await pipe.transform({ HKID: 'A0001' }, {} as any)).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
      }
    });

    it('valid HKID and case insensitive', async () => {
      const HKID = 'hw0172071 ';
      expect(await pipe.transform({ HKID }, {} as any)).toEqual({
        HKID: 'HW0172071',
      });
    });

    it('HKID undefined', async () => {
      try {
        expect(await pipe.transform(undefined, {} as any)).toThrow();
      } catch (e) {
        expect(e).toBeInstanceOf(HttpException);
      }
    });
  });
});
