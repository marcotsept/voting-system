import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IVoter, Voter } from '../schema/voter.schema';
import { VoterService } from '../voter/voter.service';
import { HttpException } from '@nestjs/common';

const mockVoter: (id?: string, HKID?: string) => Voter = (
  id = 'a cuid',
  HKID = 'A0001',
) => {
  return {
    id,
    HKID,
  };
};

describe('VoterService', () => {
  let service: VoterService;
  let model: Model<IVoter>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VoterService,
        {
          provide: getModelToken(Voter.name),
          useValue: {
            new: jest.fn().mockResolvedValue(mockVoter()),
            constructor: jest.fn().mockResolvedValue(mockVoter()),
            findOne: jest.fn(),
            create: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<VoterService>(VoterService);
    model = module.get<Model<IVoter>>(getModelToken(Voter.name));
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('create a voter not existed', async () => {
    const voter = { id: 'a cuid', HKID: 'A001' };
    jest.spyOn<Model<IVoter>, any>(model, 'findOne').mockReturnValueOnce(null);
    jest
      .spyOn<Model<IVoter>, any>(model, 'create')
      .mockReturnValueOnce(new Promise(resolve => resolve(voter)));
    const newCat = await service.create({ HKID: 'A001' });
    expect(newCat).toEqual(mockVoter('a cuid', 'A001'));
  });

  it('create a voter is existed', async () => {
    const voter = { id: 'a cuid', HKID: 'A001' };
    jest
      .spyOn<Model<IVoter>, any>(model, 'findOne')
      .mockReturnValueOnce(new Promise(resolve => resolve(voter)));
    const newCat = await service.create({ HKID: 'A001' });
    expect(newCat).toEqual(mockVoter('a cuid', 'A001'));
  });

  it('create a voter throw error', async () => {
    jest
      .spyOn<Model<IVoter>, any>(model, 'create')
      .mockRejectedValue(new Error());
    try {
      expect(await service.create({ HKID: 'A001' })).toThrow();
    } catch (err) {
      expect(err).toBeInstanceOf(HttpException);
    }
  });
});
