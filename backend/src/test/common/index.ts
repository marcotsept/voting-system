import { MongoMemoryServer } from 'mongodb-memory-server';

const mongoServer = new MongoMemoryServer({
  instance: {
    dbName: 'voting',
  },
});

export { mongoServer };
