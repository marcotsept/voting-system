import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { VoterModule } from './voter/voter.module';
import { CampaignModule } from './campaign/campaign.module';
import { VoteModule } from './vote/vote.module';
import * as toJson from '@meanie/mongoose-to-json';
import * as mongooseAutopopulate from 'mongoose-autopopulate';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    MongooseModule.forRootAsync({
      inject: [],
      useFactory: async () => ({
        uri: process.env.DB_URL,
        useCreateIndex: true,
        useNewUrlParser: true,
        connectionFactory: connection => {
          connection.plugin(toJson);
          connection.plugin(mongooseAutopopulate);
          return connection;
        },
      }),
    }),
    VoterModule,
    CampaignModule,
    VoteModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
