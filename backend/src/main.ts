import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { writeFileSync } from 'fs';
import { config } from 'dotenv';

if (process.env.NODE_ENV !== 'production') {
  config();
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const options = new DocumentBuilder()
    .setTitle('API')
    .setDescription('API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, options, {
    ignoreGlobalPrefix: false,
  });
  writeFileSync('./swagger-spec.json', JSON.stringify(document));
  SwaggerModule.setup('openapi', app, document);
  if (process.env.NODE_ENV !== 'production') app.enableCors();
  await app.listen(3001);
}
bootstrap();
