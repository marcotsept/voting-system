import { Types, Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Ref } from '../common/interfaces';
import { Candidate } from './candidate.schema';
import { ApiProperty } from '@nestjs/swagger';

export type ICampaignCandidate = CampaignCandidate & Document;

@Schema({
  collection: 'CampaignCandidate',
  versionKey: false,
  _id: false,
})
export class CampaignCandidate {
  @ApiProperty({ type: Candidate })
  @Prop({
    type: Types.ObjectId,
    required: true,
    ref: Candidate.name,
    autopopulate: true,
  })
  candidate: Ref<Candidate>;

  @ApiProperty()
  @Prop({
    type: Number,
    default: 0,
  })
  totalVotes?: number;

  @ApiProperty()
  @Prop({
    type: Number,
    required: true,
    min: 1,
  })
  seq: number;
}

const campaignCandidateSchema = SchemaFactory.createForClass(CampaignCandidate);

export { campaignCandidateSchema };
