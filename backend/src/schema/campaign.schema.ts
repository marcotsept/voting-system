import { Document, Types } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {
  CampaignCandidate,
  campaignCandidateSchema,
} from './campaign-candidate.schema';
import { ApiProperty } from '@nestjs/swagger';
export type ICampaign = Campaign & Document;

@Schema({
  collection: Campaign.name,
  versionKey: false,
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
})
export class Campaign {
  @ApiProperty()
  id?: string;

  @ApiProperty()
  @Prop({
    type: String,
    required: true,
    unique: true,
    sparse: true,
  })
  name: string;

  @ApiProperty()
  @Prop({ type: Date, required: true })
  startDate: Date;

  @ApiProperty()
  @Prop({ type: Date, required: true })
  endDate: Date;

  @ApiProperty({
    type: CampaignCandidate,
    isArray: true,
  })
  @Prop({
    type: [campaignCandidateSchema],
  })
  candidates: CampaignCandidate[];

  @ApiProperty()
  createdAt?: Date;

  @ApiProperty()
  updatedAt?: Date;
}

const campaignSchema = SchemaFactory.createForClass(Campaign);

export { campaignSchema };
