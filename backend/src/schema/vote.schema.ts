import { Types, Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Voter } from './voter.schema';
import { Ref } from '../common/interfaces';
import { Campaign } from './campaign.schema';
import { Candidate } from './candidate.schema';
import { ApiProperty } from '@nestjs/swagger';

export type IVote = Vote & Document;

@Schema({
  collection: Vote.name,
  versionKey: false,
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
})
export class Vote {
  @ApiProperty({ example: '5fd4a66c709700a64488db19' })
  id?: string;

  @ApiProperty({
    type: Voter,
  })
  @Prop({
    type: Types.ObjectId,
    required: true,
    ref: Voter.name,
    autopopulate: true,
  })
  voter: Ref<Voter>;

  @ApiProperty({
    type: Campaign,
  })
  @Prop({
    type: Types.ObjectId,
    required: true,
    ref: Campaign.name,
    autopopulate: true,
  })
  campaign: Ref<Campaign>;

  @ApiProperty({
    type: Candidate,
  })
  @Prop({
    type: Types.ObjectId,
    required: true,
    ref: Candidate.name,
    autopopulate: true,
  })
  candidate: Ref<Candidate>;

  @ApiProperty()
  createdAt?: Date;

  @ApiProperty()
  updatedAt?: Date;
}

const voteSchema = SchemaFactory.createForClass(Vote);
voteSchema.index({ campaign: 1, voter: 1 }, { unique: true });

export { voteSchema };
