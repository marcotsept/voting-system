import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export type ICandidate = Candidate & Document;

@Schema({
  collection: Candidate.name,
  versionKey: false,
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
})
export class Candidate {
  @ApiProperty()
  id?: string;

  @ApiProperty()
  @Prop({
    type: String,
    required: true,
    sparse: true,
  })
  name: string;

  @ApiProperty()
  createdAt?: Date;

  @ApiProperty()
  updatedAt?: Date;
}

const candidateSchema = SchemaFactory.createForClass(Candidate);

export { candidateSchema };
