import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export type IVoter = Voter & Document;

@Schema({
  collection: Voter.name,
  versionKey: false,
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
})
export class Voter {
  @ApiProperty({ example: '5fd4a66c709700a64488db19' })
  id?: string;

  @ApiProperty({ required: true, example: 'HW0172071' })
  @Prop({
    type: String,
    required: true,
    unique: true,
    sparse: true,
  })
  HKID: string;

  @ApiProperty()
  createdAt?: Date;

  @ApiProperty()
  updatedAt?: Date;
}

const voterSchema = SchemaFactory.createForClass(Voter);

export { voterSchema };
