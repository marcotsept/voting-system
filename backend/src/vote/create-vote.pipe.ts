import {
  ArgumentMetadata,
  HttpException,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { CreateVoteDto } from './dto';
import { yup } from '../common/yup';

@Injectable()
export class CreateVotePipe implements PipeTransform {
  async transform(value: CreateVoteDto, metadata: ArgumentMetadata) {
    const schema = yup
      .object()
      .required()
      .shape<CreateVoteDto>({
        campaign: yup
          .string()
          .required()
          .checkObjectId(),
        candidate: yup
          .string()
          .required()
          .checkObjectId(),
        voter: yup
          .string()
          .required()
          .checkObjectId(),
      });

    await schema.validate(value).catch(err => {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    });
    return value;
  }
}
