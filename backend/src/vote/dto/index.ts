import { ApiProperty } from '@nestjs/swagger';

export class CreateVoteDto {
  @ApiProperty({ required: true, example: '5fd4a66c709700a64488db19' })
  campaign: string;
  @ApiProperty({ required: true, example: '5fd4a66c709700a64488db19' })
  voter: string;
  @ApiProperty({ required: true, example: '5fd4a66c709700a64488db19' })
  candidate: string;
}
