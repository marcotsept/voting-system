import { Module } from '@nestjs/common';
import { VoteController } from './vote.controller';
import { VoteService } from './vote.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Vote, voteSchema } from '../schema/vote.schema';
import { CampaignModule } from '../campaign/campaign.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Vote.name, schema: voteSchema }]),
    CampaignModule,
  ],
  controllers: [VoteController],
  providers: [VoteService],
  exports: [VoteService],
})
export class VoteModule {}
