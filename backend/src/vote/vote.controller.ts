import { Body, Controller, Post } from '@nestjs/common';
import { VoteService } from './vote.service';
import { CreateVoteDto } from './dto';
import {
  ApiCreatedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { CreateVotePipe } from './create-vote.pipe';
import { Vote } from '../schema/vote.schema';

@ApiTags('vote')
@Controller('votes')
export class VoteController {
  constructor(private voteService: VoteService) {}

  @ApiOperation({
    description: 'Make a vote',
  })
  @ApiCreatedResponse({
    type: Vote,
  })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @Post()
  async createVote(@Body(CreateVotePipe) createVoteDto: CreateVoteDto) {
    return this.voteService.createVote(createVoteDto);
  }
}
