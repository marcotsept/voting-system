import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model, Types } from 'mongoose';
import { IVote, Vote } from '../schema/vote.schema';
import { CreateVoteDto } from './dto';
import { CampaignService } from '../campaign/campaign.service';
import moment = require('moment');
import { CampaignCandidate } from '../schema/campaign-candidate.schema';
import { Candidate } from '../schema/candidate.schema';

@Injectable()
export class VoteService {
  constructor(
    @InjectModel(Vote.name) private voteRepository: Model<IVote>,
    @InjectConnection()
    private connection: Connection,
    private campaignService: CampaignService,
  ) {}

  async createVote(createVoteDto: CreateVoteDto) {
    const campaign = await this.campaignService.getCampaignById(
      createVoteDto.campaign,
    );
    if (!campaign)
      throw new HttpException('campaign not found!', HttpStatus.BAD_REQUEST);

    if (
      campaign.candidates.findIndex((c: CampaignCandidate) => {
        return (c.candidate as Candidate).id === createVoteDto.candidate;
      }) === -1
    )
      throw new HttpException(
        'candidate does not exist on campaign!',
        HttpStatus.BAD_REQUEST,
      );

    if (moment(campaign.endDate).isBefore(moment()))
      throw new HttpException('campaign have ended!', HttpStatus.BAD_REQUEST);

    const votes = await this.voteRepository.find({
      voter: Types.ObjectId(createVoteDto.voter),
      campaign: Types.ObjectId(createVoteDto.campaign),
    });

    if (votes.length !== 0)
      throw new HttpException('you have voted!', HttpStatus.BAD_REQUEST);

    const client = this.connection.getClient();
    const session = client.startSession();
    session.startTransaction();

    await this.voteRepository.create(
      [
        {
          voter: Types.ObjectId(createVoteDto.voter),
          campaign: Types.ObjectId(createVoteDto.campaign),
          candidate: Types.ObjectId(createVoteDto.candidate),
        },
      ],
      { session },
    );
    await this.campaignService.makeVote(
      createVoteDto.campaign,
      createVoteDto.candidate,
      session,
    );

    await session.commitTransaction();
    session.endSession();

    return this.voteRepository.findOne({
      voter: Types.ObjectId(createVoteDto.voter),
      campaign: Types.ObjectId(createVoteDto.campaign),
    });
  }

  async getVotesByVoter(id: string) {
    return this.voteRepository.find({ voter: Types.ObjectId(id) });
  }
}
