import { PickType } from '@nestjs/swagger';
import { Voter } from '../../schema/voter.schema';

export class CreateVoterDto extends PickType(Voter, ['HKID'] as const) {}
