import { Module } from '@nestjs/common';
import { VoterController } from './voter.controller';
import { VoterService } from './voter.service';
import { MongooseModule } from '@nestjs/mongoose';
import { voterSchema } from '../schema/voter.schema';
import { VoteModule } from '../vote/vote.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Voter', schema: voterSchema }]),
    VoteModule,
  ],
  controllers: [VoterController],
  providers: [VoterService],
})
export class VoterModule {}
