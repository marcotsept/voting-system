import {
  ArgumentMetadata,
  HttpException,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as validid from 'validid';
import { CreateVoterDto } from './dto';
import { yup } from '../common/yup';
import { CreateVoteDto } from '../vote/dto';

@Injectable()
export class CreateVoterPipe implements PipeTransform {
  async transform(value: CreateVoterDto, metadata: ArgumentMetadata) {
    const schema = yup
      .object()
      .required()
      .shape<CreateVoterDto>({
        HKID: yup
          .string()
          .required()
          .test('check HKID', 'invalid HKID', (v: string) => {
            if (!v) return false;
            return validid.hkid(v.trim());
          }),
      });

    await schema.validate(value).catch(err => {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    });
    return { ...value, HKID: value.HKID.trim().toUpperCase() };
  }
}
