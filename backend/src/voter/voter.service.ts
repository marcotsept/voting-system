import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IVoter, Voter } from '../schema/voter.schema';
import { CreateVoterDto } from './dto';

@Injectable()
export class VoterService {
  private readonly logger = new Logger(VoterService.name);
  constructor(@InjectModel('Voter') private repository: Model<IVoter>) {}

  async create(createVoterDto: CreateVoterDto): Promise<IVoter> {
    const voter = await this.repository.findOne({ HKID: createVoterDto.HKID });
    if (voter) return voter;
    return this.repository.create({ HKID: createVoterDto.HKID }).catch(err => {
      this.logger.error(err.message);
      throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
    });
  }
}
