import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { VoterService } from './voter.service';
import { CreateVoterPipe } from './create-voter.pipe';
import {
  ApiCreatedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { VoteService } from '../vote/vote.service';
import { CreateVoterDto } from './dto';
import { Voter } from '../schema/voter.schema';
import { Vote } from '../schema/vote.schema';

@ApiTags('voter')
@Controller('voters')
export class VoterController {
  constructor(
    private voterService: VoterService,
    private voteService: VoteService,
  ) {}

  @ApiOperation({
    description: 'For create a voter by HKID, and will return existed voter',
  })
  @ApiCreatedResponse({
    type: Voter,
  })
  @ApiResponse({ status: 400, description: 'Bad Request' })
  @Post()
  async create(@Body(CreateVoterPipe) createVoterDto: CreateVoterDto) {
    return this.voterService.create(createVoterDto);
  }

  @ApiOperation({
    description: 'Get all vote by voter ID',
  })
  @ApiResponse({ status: 200, type: [Vote] })
  @Get('/:id/votes')
  async getVotes(@Param('id') id: string) {
    return this.voteService.getVotesByVoter(id);
  }
}
