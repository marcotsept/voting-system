import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';
import { config } from 'dotenv';
import { CampaignService } from './campaign.service';
import moment = require('moment');

if (process.env.NODE_ENV !== 'production') {
  config();
}
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const campaginService = app.get(CampaignService);
  const params = [
    {
      names: ['Peter Chan', 'Amy Leung', 'Victor Pang'],
      name: 'Test 101',
      startDate: moment()
        .utc()
        .add(-5, 'd')
        .startOf('d')
        .toDate(),
      endDate: moment()
        .utc()
        .add(-1, 'd')
        .startOf('d')
        .toDate(),
      totalVotes: 50,
    },
    {
      names: ['Peter Chan', 'Amy Leung', 'Victor Pang'],
      name: 'Test 102',
      startDate: moment()
        .utc()
        .add(-500, 'd')
        .startOf('d')
        .toDate(),
      endDate: moment()
        .utc()
        .add(-100, 'd')
        .startOf('d')
        .toDate(),
      totalVotes: 50,
    },
    {
      names: ['Michael Jordan', 'Kobe Bryant', 'Leborn James', 'Stephen Curry'],
      name: 'Who is the best NBA player in the history',
      startDate: moment()
        .utc()
        .year(2020)
        .month(5)
        .date(1)
        .startOf('d')
        .toDate(),
      endDate: moment()
        .utc()
        .year(2021)
        .month(5)
        .date(1)
        .startOf('d')
        .toDate(),
      totalVotes: 0,
    },
    {
      names: ['Carrie Lam', 'John Tsang', 'Rebecca Ip'],
      name: 'Which HK CEO candidate you are preferred?',
      startDate: moment()
        .utc()
        .year(2020)
        .month(1)
        .date(1)
        .startOf('d')
        .toDate(),
      endDate: moment()
        .utc()
        .year(2021)
        .month(3)
        .date(1)
        .startOf('d')
        .toDate(),
      totalVotes: 0,
    },
  ];

  try {
    for (const param of params) {
      await campaginService.createCampaignCandidateForTest(
        param.names,
        param.name,
        param.startDate,
        param.endDate,
        param.totalVotes,
      );
    }
  } catch (e) {
    console.log(e);
  } finally {
    process.exit(0);
  }
}
bootstrap();
