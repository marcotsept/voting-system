import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ClientSession, Connection, Model, Types } from 'mongoose';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Campaign, ICampaign } from '../schema/campaign.schema';
import { Candidate, ICandidate } from '../schema/candidate.schema';
import moment = require('moment');

@Injectable()
export class CampaignService {
  private readonly logger = new Logger(CampaignService.name);
  constructor(
    @InjectModel(Campaign.name) private campaignRepository: Model<ICampaign>,
    @InjectModel(Candidate.name) private candidateRepository: Model<ICandidate>,
    @InjectConnection() private connection: Connection,
  ) {}

  async createCampaignCandidateForTest(
    names = ['Peter Chan', 'Amy Leung', 'Victor Pang'],
    name = 'Who is the best NBA player in the history?',
    startDate = moment()
      .utc()
      .add(-5, 'd')
      .startOf('d')
      .toDate(),
    endDate = moment()
      .utc()
      .add(-1, 'd')
      .startOf('d')
      .toDate(),
    totalVotes = 0,
  ) {
    const candidates = await this.candidateRepository.insertMany(
      names.map(c => ({ name: c })),
    );

    return this.campaignRepository
      .create({
        name,
        startDate,
        endDate,
        candidates: candidates.map((c, idx) => ({
          totalVotes,
          candidate: c._id,
          seq: idx + 1,
        })),
      })
      .catch(err => {
        this.logger.error(err.message);
        throw new HttpException(err.message, HttpStatus.INTERNAL_SERVER_ERROR);
      });
  }

  async getAll(): Promise<ICampaign[]> {
    return this.campaignRepository.aggregate([
      { $unwind: '$candidates' },
      { $sort: { 'candidates.seq': 1 } },
      {
        $lookup: {
          from: 'Candidate',
          localField: 'candidates.candidate',
          foreignField: '_id',
          as: 'candidates.candidate',
        },
      },
      { $unwind: '$candidates.candidate' },
      {
        $addFields: {
          'candidates.candidate.id': '$candidates.candidate._id',
          dateDifference: { $subtract: ['$endDate', new Date()] },
        },
      },
      { $project: { 'candidates.candidate._id': 0 } },
      {
        $group: {
          _id: '$_id',
          id: { $first: '$_id' },
          name: { $first: '$name' },
          startDate: { $first: '$startDate' },
          endDate: { $first: '$endDate' },
          campaignTotalVotes: { $sum: '$candidates.totalVotes' },
          candidates: { $push: '$candidates' },
          createdAt: { $first: '$createdAt' },
          updatedAt: { $first: '$updatedAt' },
          dateDifference: { $first: '$dateDifference' },
        },
      },
      { $project: { _id: 0 } },
      {
        $facet: {
          nonEnd: [
            { $match: { endDate: { $gte: new Date() } } },
            { $sort: { dateDifference: 1, campaignTotalVotes: -1 } },
          ],
          end: [
            { $match: { endDate: { $lt: new Date() } } },
            { $sort: { endDate: -1, campaignTotalVotes: -1 } },
          ],
        },
      },
      {
        $addFields: {
          all: { $concatArrays: ['$nonEnd', '$end'] },
        },
      },
      { $project: { nonEnd: 0, end: 0 } },
      { $unwind: '$all' },
      {
        $replaceRoot: {
          newRoot: '$all',
        },
      },
      { $project: { campaignTotalVotes: 0, dateDifference: 0 } },
    ]);
  }

  async getCampaignById(id: string) {
    return this.campaignRepository.findOne({ _id: Types.ObjectId(id) });
  }

  async makeVote(campaign: string, candidate: string, session: ClientSession) {
    return this.campaignRepository.updateOne(
      {
        _id: Types.ObjectId(campaign),
        'candidates.candidate': Types.ObjectId(candidate),
      },
      {
        $inc: {
          'candidates.$.totalVotes': 1,
        },
      },
      { session },
    );
  }
}
