import { Controller, Get, Sse } from '@nestjs/common';
import { CampaignService } from './campaign.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { interval, Observable } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { Campaign } from '../schema/campaign.schema';

@ApiTags('campaign')
@Controller('campaigns')
export class CampaignController {
  constructor(private campaignService: CampaignService) {}

  @ApiOperation({
    description:
      'Get all campaigns, default sort by difference between current date and end date and total votes',
  })
  @ApiResponse({
    type: [Campaign],
    status: 200,
  })
  @Get('/')
  async getAll() {
    return this.campaignService.getAll();
  }

  @ApiOperation({
    description: 'SSE, get campaigns per a minute',
  })
  @ApiResponse({
    status: 200,
    content: {
      'text/event-stream': {},
    },
  })
  @Sse('/sse')
  sse(): Observable<any> {
    return interval(1000 * 60).pipe(
      concatMap(async () => ({ data: await this.campaignService.getAll() })),
    );
  }
}
