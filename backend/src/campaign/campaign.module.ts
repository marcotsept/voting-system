import { Module } from '@nestjs/common';
import { CampaignController } from './campaign.controller';
import { CampaignService } from './campaign.service';
import { MongooseModule } from '@nestjs/mongoose';
import { campaignSchema, Campaign } from '../schema/campaign.schema';
import { candidateSchema, Candidate } from '../schema/candidate.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Campaign.name, schema: campaignSchema },
    ]),
    MongooseModule.forFeature([
      { name: Candidate.name, schema: candidateSchema },
    ]),
  ],
  controllers: [CampaignController],
  providers: [CampaignService],
  exports: [CampaignService],
})
export class CampaignModule {}
