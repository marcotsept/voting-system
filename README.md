## Production
Create a network
```
docker network create voting-system
```
Go to backend folder and run the apps
```
backend$ docker-compose up -d
```
> **Note**: This also created seed data. Go to browser http://localhost:3001

> **Note**: HKID generator: https://pinkylam.me/playground/hkid/

## Development
Create a network
```
docker network create voting-system
```
Go to backend folder and run the database
```
backend$ docker-compose up -f docker-compose.debug.yml up -d
```
> **Note**: This only run a mongodb

Run the api server
```
backend$ npm run start:dev
```
> **Note**: Same of production. 3001 port is used

Go to frontend and run frontend apps
```
frontend$ npm run start
```
> **Note**: Default 3006 port for frontend

## Swagger
```
http://localhost:3001/openapi
```
> **Note**: either production or development are open.

## Unit Test
Go to backend folder
```
backend$ npm run test:cov
```
